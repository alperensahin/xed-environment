import {Log} from "./src/log";
// @ts-ignore
const config = require("../xed-config.json");

export class XedEnvironment {
    private readonly logf: Log;
    private config: any;
    constructor(service:string) {
        this.logf = new Log();
        this.log().info('Xed-environment -- ' + service);
        this.config = config;
    }
    getServicesNames(){
        return Object.keys(this.config.services);
    }
    log():Log{
        return this.logf;
    }
}
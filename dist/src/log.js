"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
class Log {
    constructor() {
    }
    info(str) {
        console.log(`[${moment().format('DD/MM/YYYY')}][${moment().format('hh:mm:ss')}][\x1b[36m%s\x1b[0m] : ${str}`, 'INFO');
    }
    error(str) {
        console.log(`[${moment().format('DD/MM/YYYY')}][${moment().format('hh:mm:ss')}][\x1b[31m%s\x1b[0m] : ${str}`, 'ERROR');
    }
}
exports.Log = Log;

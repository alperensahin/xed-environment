export declare class Log {
    constructor();
    info(str: string): void;
    error(str: string): void;
}

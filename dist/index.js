"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const log_1 = require("./src/log");
// @ts-ignore
const config = require("../xed-config.json");
class XedEnvironment {
    constructor(service) {
        this.logf = new log_1.Log();
        this.log().info('Xed-environment -- ' + service);
        this.config = config;
    }
    getServicesNames() {
        return Object.keys(this.config.services);
    }
    log() {
        return this.logf;
    }
}
exports.XedEnvironment = XedEnvironment;

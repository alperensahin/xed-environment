import { Log } from "./src/log";
export declare class XedEnvironment {
    private readonly logf;
    private config;
    constructor(service: string);
    getServicesNames(): string[];
    log(): Log;
}

import moment  =require("moment");

export class Log {
    constructor() {

    }
    info(str: string) {
        console.log(`[${moment().format('DD/MM/YYYY')}][${moment().format('hh:mm:ss')}][\x1b[36m%s\x1b[0m] : ${str}`, 'INFO');
    }
    error(str : string) {
        console.log(`[${moment().format('DD/MM/YYYY')}][${moment().format('hh:mm:ss')}][\x1b[31m%s\x1b[0m] : ${str}`, 'ERROR');
    }
}